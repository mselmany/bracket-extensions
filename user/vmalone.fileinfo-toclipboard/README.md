File Info to Clipboard
===============

Adds options to copy a file's - name, path, and directory path - to the clipboard.


### To Use ###

Right click on a file in the left sidebar panel and select "_X_ to clipboard" or go to "File > _X_ to clipboard".


### Notes ###

- The extension is similar to Notepad++'s functionality.
- Uses copy/paste library from:
 - https://github.com/xavi-/node-copy-paste