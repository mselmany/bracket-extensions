/*
* mselmany-theme-light - for UI changes overrides
*/

define(function (require, exports, module) {
    "use strict";
    
    var AppInit                     = brackets.getModule("utils/AppInit"),
        ExtensionUtils              = brackets.getModule("utils/ExtensionUtils");

 

    //while brakets starts
    AppInit.appReady(function () {
        ExtensionUtils.loadStyleSheet(module, "css/main.css");
    });
    
});